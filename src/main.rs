#[macro_use]
extern crate log;

use sqlx::postgres::{PgPool, PgPoolOptions};

use tokio::sync::Mutex;

use serenity::{
    async_trait,
    client::Client,
    framework::standard::{
        macros::{command, group, hook},
        Args, CommandResult, StandardFramework,
    },
    model::{channel::Message, gateway::Ready, id::ChannelId},
    prelude::{Context, EventHandler, TypeMapKey},
};

use std::collections::BTreeMap;
use std::fs::File;
use std::io::Read;

use serenity::model::prelude::*;

pub mod commands;

use commands::{hivemind::HIVEMIND_COMMAND, impersonate::IMPERSONATE_COMMAND};

struct ConnectionPool;
struct IngestLimit;

impl TypeMapKey for ConnectionPool {
    type Value = PgPool;
}

impl TypeMapKey for IngestLimit {
    type Value = i64;
}

#[command]
async fn ping(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    if let Err(why) = msg.channel_id.say(&ctx.http, "Pong!").await {
        warn!("Error sending message: {:?}", why);
    }
    Ok(())
}

/// Usage: `~configure limit mentions?`
/// Example: `~configure 100 false`
/// This will allow a maximum of 100 messages for hivemind and impersonate
/// and also will disallow mentions to be used as usernames for impersonate.
///
/// Only the first parameter is required, and setting it to 0 will remove the limit.
/// The second parameter is not required, but it will default to True if not mentioned.
#[command]
#[only_in("guilds")]
#[required_permissions(MANAGE_GUILD)]
#[min_args(1)]
#[aliases(config, conf)]
async fn configure(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let pool = {
        let data_read = ctx.data.read().await;
        data_read.get::<ConnectionPool>().unwrap().clone()
    };

    // the trait `std::str::FromStr` is not implemented for `std::option::Option<_>`
    // :sleep:
    let limit = args.single_quoted::<i64>().unwrap_or(0).abs();
    let mention = args.single_quoted().unwrap_or(true);

    let post_limit = if limit == 0 { None } else { Some(limit) };

    let guild_id = msg.guild_id.unwrap().0 as i64;

    if let Err(why) = sqlx::query!("INSERT INTO guilds (id, allow_mention, post_limit) VALUES ($1, $2, $3) ON CONFLICT (id) DO UPDATE SET allow_mention=$2, post_limit=$3", guild_id, mention, post_limit)
    .execute(&pool)
    .await {
        error!("Error inserting message to database: {}", why);
    };

    msg.channel_id.say(&ctx, "Success!").await?;

    Ok(())
}

#[command]
async fn stats(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    let pool = {
        let data_read = ctx.data.read().await;
        data_read.get::<ConnectionPool>().unwrap().clone()
    };

    let cache = ctx.cache.clone();
    let mut guild_names: Vec<String> = Vec::new();

    for x in cache.guilds().await.clone() {
        guild_names.push(x.to_partial_guild(&ctx.http).await.unwrap().name);
    }

    let unique_users_query =
        sqlx::query!("SELECT COUNT(*) FROM (SELECT DISTINCT author FROM messages) AS temp")
            .fetch_optional(&pool)
            .boxed()
            .await?;

    let unique_channels_query =
        sqlx::query!("SELECT COUNT(*) FROM (SELECT DISTINCT channel_id FROM messages) AS temp")
            .fetch_optional(&pool)
            .boxed()
            .await?;

    let unique_users = if let Some(result) = unique_users_query {
        if let Some(x) = result.count {
            Some(x)
        } else {
            None
        }
    } else {
        None
    };

    let unique_channels = if let Some(result) = unique_channels_query {
        if let Some(x) = result.count {
            Some(x)
        } else {
            None
        }
    } else {
        None
    };

    info!("guilds: ({}) {:?}", guild_names.len(), guild_names);

    if let Err(why) = msg
        .channel_id
        .say(
            &ctx.http,
            format!(
                "guilds currently in: {}; channels: {}; users: {}",
                guild_names.len(),
                unique_channels.unwrap_or(-1),
                unique_users.unwrap_or(-1)
            ),
        )
        .await
    {
        info!("Error sending message: {:?}", why);
    };
    Ok(())
}

// Defines the handler to be used for events.
#[derive(Debug)]
struct Handler {
    run_loops: Mutex<bool>,
}

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, _: Context, ready: Ready) {
        info!("Version {} of markovbot", env!("CARGO_PKG_VERSION"));
        info!("{} is connected!", ready.user.name);
    }

    async fn resume(&self, _: Context, resume: ResumedEvent) {
        debug!("Resumed; trace: {:?}", resume.trace);
    }

    async fn guild_create(&self, ctx: Context, guild: Guild, _: bool) {
        if let Err(why) = commands::helper::download_all_messages(&guild, &ctx).await {
            error!("Error downloading messages: {}", why);
        };
    }

    async fn message(&self, ctx: Context, message: Message) {
        let pool = {
            let data_read = ctx.data.read().await;
            data_read.get::<ConnectionPool>().unwrap().clone()
        };

        let attachment_urls = if message.attachments.is_empty() {
            None
        } else {
            let mut urls: Vec<String> = vec![];
            for attachment in message.attachments {
                urls.push(attachment.url);
            }
            Some(urls)
        };

        let g_id = message.guild_id.unwrap().0 as i64;
        let message_id = message.id.0 as i64;
        let channel_id = message.channel_id.0 as i64;
        let author_id = message.author.id.0 as i64;
        let content = &message.content;
        let timestamp = message.timestamp;

        if let Err(why) = sqlx::query!("INSERT INTO messages (id, channel_id, author, content, timestamp, guild_id, attachment_urls) VALUES ($1, $2, $3, $4, $5, $6, $7) ON CONFLICT (id) DO UPDATE SET content=$4",
                message_id, channel_id, author_id, content, timestamp, g_id, &attachment_urls.unwrap_or_default() )
            .execute(&pool)
            .await
        {
            error!("Error inserting message to database: {}", why);
        };

        // debug!("message: {:?}", message);

        //println!("added message on_message: {}", message.id.0.to_string());
    }

    async fn message_update(
        &self,
        ctx: Context,
        _old_if_available: Option<Message>,
        new: Option<Message>,
        _event: MessageUpdateEvent,
    ) {
        if new.is_none() {
            return;
        }
        let message = new.unwrap();

        let pool = {
            let data_read = ctx.data.read().await;
            data_read.get::<ConnectionPool>().unwrap().clone()
        };

        let attachment_urls = if message.attachments.is_empty() {
            None
        } else {
            let mut urls: Vec<String> = vec![];
            for attachment in message.attachments {
                urls.push(attachment.url);
            }
            Some(urls)
        };

        let g_id = message.guild_id.unwrap().0 as i64;
        let message_id = message.id.0 as i64;
        let channel_id = message.channel_id.0 as i64;
        let author_id = message.author.id.0 as i64;
        let content = &message.content;
        let timestamp = message.timestamp;

        if let Err(why) = sqlx::query!("INSERT INTO messages (id, channel_id, author, content, timestamp, guild_id, attachment_urls) VALUES ($1, $2, $3, $4, $5, $6, $7) ON CONFLICT (id) DO UPDATE SET content=$4, timestamp=$5",
                message_id, channel_id, author_id, content, timestamp, g_id, &attachment_urls.unwrap_or_default() )
            .execute(&pool)
            .await
        {
            error!("Error inserting message to database: {}", why);
        };

        // debug!("message: {:?}", message);

        // debug!("update message: {}", message.id.0.to_string());
    }

    async fn message_delete(
        &self,
        ctx: Context,
        _channel_id: ChannelId,
        deleted_message_id: MessageId,
    ) {
        let pool = {
            let data_read = ctx.data.read().await;
            data_read.get::<ConnectionPool>().unwrap().clone()
        };

        if let Err(why) = sqlx::query!(
            "DELETE FROM messages WHERE id=$1",
            deleted_message_id.0 as i64
        )
        .execute(&pool)
        .await
        {
            error!("Error deleting message from database: {}", why);
        };

        // debug!("deleted message: {}", deleted_message_id.0.to_string());
    }
}

#[hook]
async fn before(_ctx: &Context, msg: &Message, cmd_name: &str) -> bool {
    info!("Running command: {}", &cmd_name);
    debug!("Message: {}", &msg.content);

    true
}

#[group("General")]
#[commands(ping, stats, impersonate, hivemind, configure)]
struct General;

#[tokio::main(core_threads = 8)]

async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    pretty_env_logger::init();

    let mut f = File::open("config.yaml").unwrap();
    let mut fstr = String::new();
    let _ = f.read_to_string(&mut fstr);

    let config: BTreeMap<String, String> = serde_yaml::from_str(&fstr).unwrap();

    let dbname = config["db"].clone();

    let pool = PgPoolOptions::new()
        .max_connections(20)
        .connect(&dbname)
        .await?;

    info!("pre-init done");

    let std_framework = StandardFramework::new()
        .configure(|c| c.prefix("~")) // set the bot's prefix to "~"
        .before(before)
        .group(&GENERAL_GROUP);

    let mut client = Client::new(&config["token"])
        .event_handler(Handler {
            run_loops: Mutex::new(true),
        })
        .framework(std_framework)
        .await?;

    {
        let mut data = client.data.write().await;
        data.insert::<ConnectionPool>(pool);
        data.insert::<IngestLimit>(
            config
                .get("ingest_limit")
                .clone()
                .unwrap_or(&"10000".to_owned())
                .parse::<i64>()
                .unwrap_or(10000),
        );
    }

    if let Err(why) = client.start_autosharded().await {
        println!("Client error: {:?}", why);
    };

    Ok(())
}
