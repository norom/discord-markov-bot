use markov::Chain;
use regex::Regex;
use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::{channel::Message, id::UserId},
    prelude::Context,
    utils::{content_safe, ContentSafeOptions}, // To change the embed help color
};

use futures::TryStreamExt;

use crate::ConnectionPool;
use crate::IngestLimit;

enum IdOrUsername {
    Id(u64),
    Username(String),
}

#[command]
#[min_args(1)]
pub async fn impersonate(ctx: &Context, message: &Message, mut args: Args) -> CommandResult {
    let _ = message.channel_id.broadcast_typing(&ctx.http).await;

    debug!("args: {:?}", args);

    let pool = {
        let data_read = ctx.data.read().await;
        data_read.get::<ConnectionPool>().unwrap().clone()
    };

    let ingest_limit = {
        let data_read = ctx.data.read().await;
        data_read.get::<IngestLimit>().unwrap().to_owned()
    };

    let guild_results = {
        sqlx::query!(
            "SELECT allow_mention, post_limit FROM guilds WHERE id=$1 LIMIT 1",
            message.guild_id.unwrap().0 as i64
        )
        .fetch_optional(&pool)
        .await
        .unwrap()
    };

    let (allow_mention, limit) = {
        if let Some(x) = guild_results {
            (x.allow_mention, x.post_limit)
        } else {
            (true, None)
        }
    };

    let fetch_from = match args.single::<u64>() {
        Ok(id) => IdOrUsername::Id(id),
        Err(_) => {
            let user = args.single_quoted().unwrap_or_else(|_| "".to_owned());
            if user.starts_with("<@") && user.ends_with('>') && allow_mention {
                let re = Regex::new("[<@!>]").unwrap();
                let user_id = re.replace_all(&user, "").into_owned();
                match user_id.parse::<u64>() {
                    Ok(id) => IdOrUsername::Id(id),
                    Err(_) => IdOrUsername::Username(user),
                }
            } else {
                IdOrUsername::Username(user)
            }
        }
    };

    let count: usize = args.single_quoted().unwrap_or(1);

    if let Some(l) = limit {
        if (l as usize) < count {
            message
                .channel_id
                .say(&ctx, format!("You went past the limit of {} messages", l))
                .await?;
            return Ok(());
        }
    }

    // let chan = message.channel_id.get().unwrap();

    if let Some(user) = match fetch_from {
        IdOrUsername::Id(id) => Some(UserId(id)),
        IdOrUsername::Username(username) => message
            .guild(&ctx.cache)
            .await
            .unwrap()
            .member_named(&username)
            .map(|m| m.user.id),
    } {
        let mut chain: Chain<String> = Chain::new();

        let mut results = sqlx::query!("SELECT content, attachment_urls FROM messages WHERE author=$1 AND content NOT LIKE '%~hivemind%' AND content NOT LIKE '%~impersonate%' AND content NOT LIKE '%~ping%' AND content NOT LIKE '%~stats%' ORDER BY RANDOM() LIMIT $2 ", user.0 as i64, ingest_limit)
            .fetch(&pool);

        let mut fed_chain = false;

        while let Some(m) = results.try_next().await? {
            trace!(
                "Feeding message '{}' with attachment_urls '{:?}' into chain",
                m.content,
                m.attachment_urls
            );
            let in_str = if m.attachment_urls.is_some() {
                m.content + "\n" + &m.attachment_urls.unwrap().join("\n")
            } else {
                m.content
            };

            if !in_str.is_empty() {
                chain.feed_str(&in_str);
                fed_chain = true;
            }
        }

        if !fed_chain {
            info!("Requested command has no data available");
            let _ = message
                .reply(
                    &ctx,
                    "Either they've never said anything, or I haven't seen them",
                )
                .await;
        }

        let _ = message.channel_id.broadcast_typing(&ctx.http).await;

        for mut line in chain.str_iter_for(count) {
            trace!("Outgoing message: '{}'", line);

            while line.is_empty() {
                line = chain.generate_str();
            }

            let _ = message
                .channel_id
                .say(
                    &ctx.http,
                    content_safe(&ctx.cache, &line, &ContentSafeOptions::default()).await,
                )
                .await;
        }
    } else {
        info!("User not found");
        let _ = message.reply(&ctx, "No user found").await;
    }
    Ok(())
}
