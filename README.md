Dev: [![build status](https://gitlab.com/tahahawa/discord-markov-bot/badges/dev/build.svg)](https://gitlab.com/tahahawa/discord-markov-bot/commits/dev)

Master : [![build status](https://gitlab.com/tahahawa/discord-markov-bot/badges/master/build.svg)](https://gitlab.com/tahahawa/discord-markov-bot/commits/master)

Discord bot that analyses all users' messages, markov chains them to "impersonate" people

Link to add the "official" instance to your server: https://discordapp.com/oauth2/authorize?client_id=276839250186469376&scope=bot&permissions=67185664

### How to run:

- Install diesel_cli: `cargo install diesel_cli --no-default-features --features "postgres"`
- Rename `config.yaml.example` to `config.yaml` and fill in the required information.
- Configure the database: `diesel setup --database-url='postgres://localhost/my_db'` (the url is the same as in the configuration file)
- `cargo run --release`
